import 'package:flutter/cupertino.dart';
import '../utils/includes.dart';

enum ScreenSection { lower, upper }

class CurrencyConverter {
  static final CurrencyConverter instance = CurrencyConverter();

  void alertMessage({
    required BuildContext context,
    required ScreenSection screenSection,
    required Currency fromCurrency,
    required Currency toCurrency,
  }) {
    Navigator.of(context).pushReplacement(
      CupertinoPageRoute(
        fullscreenDialog: true,
        builder: (context) {
          fromCurrency.value = 0;
          toCurrency.value = 0;
          return instance.goToHomeScreen(
            screenSection: screenSection,
            fromCurrency: fromCurrency,
            toCurrency: toCurrency,
          );
        },
      ),
    );
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          title: const Text(
            "Oops !",
            style: TextStyle(color: Color(0xff444444)),
          ),
          content: const Text(
            "Feature not implemented yet, COMMING SOON ! 😎",
            style: TextStyle(
                height: 1.5,
                fontWeight: FontWeight.w300,
                fontSize: 15,
                color: Color(0xff444444)),
          ),
          titlePadding: const EdgeInsets.only(left: 20, top: 10, bottom: 10),
          insetPadding: const EdgeInsets.all(20),
          // alignment: Alignment.bottomCenter,
          contentPadding: const EdgeInsets.symmetric(horizontal: 20),
          actionsPadding: const EdgeInsets.only(right: 20, bottom: 10),
          actions: [
            MaterialButton(
              colorBrightness: Brightness.light,
              color: AppStyles.themeColor,
              onPressed: (() => Navigator.pop(context)),
              child: const Text(
                "Okay",
                style: TextStyle(color: Colors.white),
              ),
            )
          ],
        );
      },
    );
  }

  goToHomeScreen({
    required ScreenSection screenSection,
    required Currency fromCurrency,
    required Currency toCurrency,
  }) {
    if (screenSection == ScreenSection.upper) {
      return HomeScreen(
        upperSection: fromCurrency,
        lowerSection: toCurrency,
        isWhite: false,
      );
    }
    return HomeScreen(
      upperSection: toCurrency,
      lowerSection: fromCurrency,
      isWhite: true,
    );
  }

  static void convertCurrency({
    required BuildContext context,
    required Currency fromCurrency,
    required Currency toCurrency,
    required double amount,
    ScreenSection screenSection = ScreenSection.upper,
  }) {
    var fromCurrencyNotation = fromCurrency.notation.toUpperCase();
    var toCurrencyNotation = toCurrency.notation.toUpperCase();

    if ((fromCurrencyNotation.compareTo(toCurrencyNotation)) == 0) {
      Navigator.of(context).pushReplacement(
        CupertinoPageRoute(
          fullscreenDialog: true,
          builder: (context) {
            fromCurrency.value = amount;
            toCurrency.value = amount;
            return HomeScreen(
              upperSection: toCurrency,
              lowerSection: fromCurrency,
              isWhite: true,
            );
          },
        ),
      );
    } else if (fromCurrency.notation.toUpperCase() == "USD") {
      switch (toCurrency.notation.toUpperCase()) {
        case "KSH":
          Navigator.of(context).pushReplacement(
            CupertinoPageRoute(
              fullscreenDialog: true,
              builder: (context) {
                fromCurrency.value = amount;
                toCurrency.value = amount * 124;
                return instance.goToHomeScreen(
                  screenSection: screenSection,
                  fromCurrency: fromCurrency,
                  toCurrency: toCurrency,
                );
              },
            ),
          );
          break;
        default:
          instance.alertMessage(
            context: context,
            fromCurrency: fromCurrency,
            screenSection: screenSection,
            toCurrency: toCurrency,
          );
      }
    } else if (fromCurrency.notation.toUpperCase() == "KSH") {
      switch (toCurrency.notation.toUpperCase()) {
        case "USD":
          Navigator.of(context).pushReplacement(
            CupertinoPageRoute(
              fullscreenDialog: true,
              builder: (context) {
                fromCurrency.value = amount;
                toCurrency.value = amount / 124;
                return instance.goToHomeScreen(
                  screenSection: screenSection,
                  fromCurrency: fromCurrency,
                  toCurrency: toCurrency,
                );
              },
            ),
          );
          break;
        case "TSH":
          Navigator.of(context).pushReplacement(
            CupertinoPageRoute(
              fullscreenDialog: true,
              builder: (context) {
                fromCurrency.value = amount;
                toCurrency.value = amount * 50;
                return instance.goToHomeScreen(
                  screenSection: screenSection,
                  fromCurrency: fromCurrency,
                  toCurrency: toCurrency,
                );
              },
            ),
          );
          break;
        default:
          instance.alertMessage(
            context: context,
            fromCurrency: fromCurrency,
            screenSection: screenSection,
            toCurrency: toCurrency,
          );
      }
    } else if (fromCurrencyNotation == "TSH") {
      switch (toCurrencyNotation) {
        case "KSH":
          Navigator.of(context).pushReplacement(
            CupertinoPageRoute(
              fullscreenDialog: true,
              builder: (context) {
                fromCurrency.value = amount;
                toCurrency.value = amount / 50;
                return instance.goToHomeScreen(
                  screenSection: screenSection,
                  fromCurrency: fromCurrency,
                  toCurrency: toCurrency,
                );
              },
            ),
          );
          break;
        default:
          instance.alertMessage(
            context: context,
            fromCurrency: fromCurrency,
            screenSection: screenSection,
            toCurrency: toCurrency,
          );
      }
    } else {
      instance.alertMessage(
        context: context,
        fromCurrency: fromCurrency,
        screenSection: screenSection,
        toCurrency: toCurrency,
      );
    }
  }
}
