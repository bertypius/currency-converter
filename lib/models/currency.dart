class Currency {
  final int id;
  final String name;
  final String notation;
  double value;

  Currency({
    required this.id,
    required this.value,
    required this.name,
    required this.notation,
  });

  static List<Currency> currencyList = [
    Currency(id: 1, name: "Indian Rupee", notation: "INR", value: 0),
    Currency(id: 2, name: "Japanese Yen", notation: "JPY", value: 0),
    Currency(id: 3, name: "Kenyan Shilling", notation: "KSH", value: 0),
    Currency(id: 4, name: "Pound Sterling", notation: "GBP", value: 0),
    Currency(id: 5, name: "Russian Ruble", notation: "RUB", value: 0),
    Currency(id: 6, name: "Tanzanian Shilling", notation: "TSH", value: 0),
    Currency(id: 7, name: "Ugandan Shilling", notation: "USH", value: 0),
    Currency(id: 8, name: "United States Dollars", notation: "USD", value: 0),
  ];

  static List<Currency> searchCurrency(String keyword) {
    var result = currencyList
        .where((element) =>
            element.name.toLowerCase().contains(keyword.toLowerCase()))
        .toList();
    return result;
  }
}
