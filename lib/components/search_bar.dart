import '../utils/includes.dart';

class SearchBar extends StatelessWidget {
  final Color color;
  final Function filterCurrencies;
  const SearchBar({
    super.key,
    required this.color,
    required this.filterCurrencies,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 80,
      padding: const EdgeInsets.only(bottom: 10.0),
      child: Stack(
        children: [
          Container(
            height: 50,
            decoration: BoxDecoration(
              color: color,
              borderRadius:
                  const BorderRadius.vertical(bottom: Radius.circular(30)),
            ),
          ),
          Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            child: Padding(
              padding: const EdgeInsets.only(left: 20.0, right: 20.0),
              child: Container(
                decoration: BoxDecoration(boxShadow: [
                  BoxShadow(
                    blurRadius: 20,
                    color: Colors.black.withOpacity(0.13),
                    offset: const Offset(1, 4),
                  )
                ]),
                child: TextField(
                  onChanged: (value) {
                    filterCurrencies(value);
                  },
                  cursorColor: Colors.black12,
                  decoration: InputDecoration(
                    contentPadding:
                        const EdgeInsets.symmetric(vertical: 0, horizontal: 20),
                    hintText: "Search currency ...",
                    hintStyle: const TextStyle(
                      fontWeight: FontWeight.w300,
                    ),
                    suffixIcon: Padding(
                      padding: const EdgeInsets.all(4.0),
                      child: CircleAvatar(
                        backgroundColor: AppStyles.themeColor.withOpacity(.15),
                        child: Icon(
                          Icons.search,
                          size: 20,
                          color: AppStyles.themeColor,
                        ),
                      ),
                    ),
                    constraints: const BoxConstraints(maxHeight: 45),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(30),
                        borderSide: BorderSide.none),
                    fillColor: Colors.white,
                    filled: true,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
