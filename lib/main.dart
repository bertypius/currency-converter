import 'utils/includes.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
  ]).then((value) => runApp(const MyApp()));
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(primaryColor: AppStyles.themeColor),
      debugShowCheckedModeBanner: false,
      title: "Currency Converter Application",
      initialRoute: HomeScreen.routeName,
      routes: routes,
    );
  }
}
