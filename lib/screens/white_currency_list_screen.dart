import '../utils/includes.dart';

class WhiteCurrencyList extends StatefulWidget {
  const WhiteCurrencyList({super.key});

  @override
  State<WhiteCurrencyList> createState() => _WhiteCurrencyListState();
}

class _WhiteCurrencyListState extends State<WhiteCurrencyList> {
  ValueNotifier<List<Currency>?> currencyList = ValueNotifier(null);

  @override
  void initState() {
    currencyList.value = Currency.currencyList;
    super.initState();
  }

  void filterCurrencies(String value) {
    var result = Currency.searchCurrency(value);
    currencyList.value = result;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      appBar: AppBar(
        systemOverlayStyle: SystemUiOverlayStyle(
          statusBarColor: AppStyles.themeColor,
          statusBarIconBrightness: Brightness.light,
        ),
        elevation: 0,
        backgroundColor: AppStyles.themeColor,
        leading: InkWell(
          onTap: () => Navigator.pop(context),
          child: Padding(
            padding: const EdgeInsets.only(left: 20),
            child: CircleAvatar(
              backgroundColor: Colors.black.withOpacity(.25),
              child: const Icon(
                Icons.arrow_back,
                color: Colors.white,
                size: 17,
              ),
            ),
          ),
        ),
        title: const Text(
          "Select currency from the list",
          style: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.w500,
            color: Colors.white,
          ),
        ),
      ),
      body: Column(
        children: [
          SearchBar(
            color: AppStyles.themeColor,
            filterCurrencies: filterCurrencies,
          ),
          Expanded(
            child: ValueListenableBuilder(
              valueListenable: currencyList,
              builder: (context, currencyList, child) {
                return ListView.separated(
                  separatorBuilder: (BuildContext context, int index) {
                    return const Divider();
                  },
                  padding:
                      const EdgeInsets.symmetric(horizontal: 25, vertical: 10),
                  itemCount: currencyList!.length,
                  itemBuilder: (context, index) {
                    var item = currencyList[index];
                    return ListTile(
                      visualDensity: VisualDensity.comfortable,
                      onTap: () {
                        Navigator.pop(context, item);
                      },
                      leading: Text(
                        item.name,
                        style: const TextStyle(
                          color: Colors.black87,
                          fontSize: 17,
                          fontWeight: FontWeight.w300,
                        ),
                      ),
                      title: Chip(
                        padding: const EdgeInsets.symmetric(vertical: 0),
                        visualDensity: VisualDensity.compact,
                        labelPadding: const EdgeInsets.symmetric(
                            vertical: 0, horizontal: 15),
                        backgroundColor: AppStyles.themeColor.withOpacity(.15),
                        label: Text(
                          item.notation,
                          style: TextStyle(
                            color: AppStyles.themeColor,
                            fontSize: 10,
                            letterSpacing: 1,
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                      ),
                      trailing: const Icon(
                        Icons.chevron_right,
                        color: Colors.grey,
                        size: 17,
                      ),
                    );
                  },
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
