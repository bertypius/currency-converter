import '../utils/includes.dart';

class WhiteInputScreen extends StatefulWidget {
  static String routeName = "whiteInputScreen";
  final Currency fromCurrency;
  final Currency toCurrency;

  const WhiteInputScreen({
    super.key,
    required this.fromCurrency,
    required this.toCurrency,
  });

  @override
  State<WhiteInputScreen> createState() => _WhiteInputScreenState();
}

class _WhiteInputScreenState extends State<WhiteInputScreen> {
  var inputValue = ValueNotifier(0);
  var inputController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppStyles.themeColor,
        elevation: 0,
        toolbarHeight: 0,
        systemOverlayStyle: SystemUiOverlayStyle(
          statusBarIconBrightness: Brightness.dark,
          statusBarColor: Theme.of(context).scaffoldBackgroundColor,
        ),
      ),
      body: Padding(
        padding:
            const EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 40),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Padding(
                  padding: EdgeInsets.all(12.0),
                  child: InkWell(
                    child: Icon(Icons.menu),
                    // onTap: () => Navigator.pop(context)
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 15, top: 5),
                  child: InputChip(
                    backgroundColor: Colors.black.withOpacity(0.75),
                    label: const Text('Clear input',
                        style: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.w300)),
                    onPressed: () {
                      inputValue.value = 0;
                    },
                  ),
                ),
              ],
            ),
            ValueListenableBuilder(
              valueListenable: inputValue,
              builder: (context, value, child) {
                return Text(value.toString(),
                    style: Theme.of(context).textTheme.headlineMedium);
              },
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  width: 270,
                  child: Wrap(
                    alignment: WrapAlignment.center,
                    crossAxisAlignment: WrapCrossAlignment.end,
                    spacing: 60,
                    runSpacing: 35,
                    children: List.generate(
                      12,
                      (index) {
                        if (index == 9) {
                          return btnNumber(
                            index: (index + 1),
                            child: const Icon(Icons.clear,
                                color: Colors.white, size: 18),
                          );
                        }
                        if (index == 10) {
                          return btnNumber(
                            index: 0,
                            child: const Text(
                              "0",
                              style:
                                  TextStyle(color: Colors.white, fontSize: 17),
                            ),
                          );
                        }
                        if (index == 11) {
                          return btnNumber(
                            index: index,
                            child: const Icon(Icons.check,
                                color: Colors.white, size: 18),
                          );
                        }
                        return btnNumber(
                          index: index + 1,
                          child: Text(
                            (index + 1).toString(),
                            style: const TextStyle(
                                color: Colors.white, fontSize: 17),
                          ),
                        );
                      },
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  GestureDetector btnNumber({required Widget child, required int index}) {
    return GestureDetector(
      onTap: () {
        if (index == 10) {
          String x = inputController.text;
          int length = inputController.text.length;
          if (length - 1 >= 1) {
            x = inputController.text.substring(0, length - 1);
            inputValue.value = int.parse(x);
            inputController.text = x;
          } else {
            inputController.text = "";
            inputValue.value = 0;
          }
        } else if (index == 11) {
          CurrencyConverter.convertCurrency(
            context: context,
            amount: inputValue.value.toDouble(),
            toCurrency: widget.toCurrency,
            fromCurrency: widget.fromCurrency,
            screenSection: ScreenSection.lower,
          );
        } else {
          inputValue.value = (inputValue.value * 10) + index;
          inputController.text = (inputValue.value).toString();
        }
      },
      child: Container(
        width: 50,
        height: 50,
        decoration: const BoxDecoration(
          shape: BoxShape.circle,
          color: Color(0xeeec5759),
        ),
        child: Center(child: child),
      ),
    );
  }
}
