import '../utils/includes.dart';

class RedInputScreen extends StatefulWidget {
  static String routeName = "redInputScreen";
  final Currency fromCurrency;
  final Currency toCurrency;

  const RedInputScreen({
    super.key,
    required this.fromCurrency,
    required this.toCurrency,
  });

  @override
  State<RedInputScreen> createState() => _RedInputScreenState();
}

class _RedInputScreenState extends State<RedInputScreen> {
  var inputValue = ValueNotifier(0);
  var inputController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppStyles.themeColor,
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Padding(
                  padding: EdgeInsets.all(12.0),
                  child: InkWell(
                    child: Icon(Icons.menu),
                    // onTap: () => Navigator.pop(context)
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 15, top: 5),
                  child: InputChip(
                    backgroundColor: Colors.black.withOpacity(0.75),
                    label: const Text('Clear input',
                        style: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.w300)),
                    onPressed: () {
                      inputValue.value = 0;
                    },
                  ),
                ),
              ],
            ),
            ValueListenableBuilder(
              valueListenable: inputValue,
              builder: (context, value, child) {
                return Text(value.toString(),
                    style: Theme.of(context).textTheme.headlineMedium);
              },
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  width: 270,
                  child: Wrap(
                    alignment: WrapAlignment.center,
                    crossAxisAlignment: WrapCrossAlignment.end,
                    spacing: 60,
                    runSpacing: 35,
                    children: List.generate(
                      12,
                      (index) {
                        if (index == 9) {
                          return btnNumber(
                            index: (index + 1),
                            child: const Icon(Icons.clear,
                                color: Colors.white, size: 18),
                          );
                        }
                        if (index == 10) {
                          return btnNumber(
                            index: 0,
                            child: const Text(
                              "0",
                              style:
                                  TextStyle(color: Colors.white, fontSize: 17),
                            ),
                          );
                        }
                        if (index == 11) {
                          return btnNumber(
                            index: index,
                            child: const Icon(Icons.check,
                                color: Colors.white, size: 18),
                          );
                        }
                        return btnNumber(
                          index: index + 1,
                          child: Text(
                            (index + 1).toString(),
                            style: const TextStyle(
                                color: Colors.white, fontSize: 17),
                          ),
                        );
                      },
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  GestureDetector btnNumber({required Widget child, required int index}) {
    return GestureDetector(
      onTap: () {
        if (index == 10) {
          String x = inputController.text;
          int length = inputController.text.length;
          if (length - 1 >= 1) {
            x = inputController.text.substring(0, length - 1);
            inputValue.value = int.parse(x);
            inputController.text = x;
          } else {
            inputController.text = "";
            inputValue.value = 0;
          }
        } else if (index == 11) {
          CurrencyConverter.convertCurrency(
            context: context,
            amount: inputValue.value.toDouble(),
            toCurrency: widget.toCurrency,
            fromCurrency: widget.fromCurrency,
          );
        } else {
          inputValue.value = (inputValue.value * 10) + index;
          inputController.text = (inputValue.value).toString();
        }
      },
      child: Container(
        width: 50,
        height: 50,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: Colors.white.withOpacity(.3),
        ),
        child: Center(child: child),
      ),
    );
  }
}
