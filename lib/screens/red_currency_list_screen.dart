import '../utils/includes.dart';

class RedCurrencyList extends StatefulWidget {
  const RedCurrencyList({super.key});

  @override
  State<RedCurrencyList> createState() => _RedCurrencyListState();
}

class _RedCurrencyListState extends State<RedCurrencyList> {
  ValueNotifier<List<Currency>?> currencyList = ValueNotifier(null);
  @override
  void initState() {
    currencyList.value = Currency.currencyList;
    super.initState();
  }

  void filterCurrencies(String value) {
    var result = Currency.searchCurrency(value);
    currencyList.value = result;
  }

  @override
  Widget build(BuildContext context) {
    debugPrint("Building RedCurrencyList...");
    return Scaffold(
      backgroundColor: AppStyles.themeColor,
      appBar: AppBar(
        systemOverlayStyle: SystemUiOverlayStyle(
          statusBarColor: Theme.of(context).scaffoldBackgroundColor,
          statusBarIconBrightness: Brightness.dark,
        ),
        elevation: 0,
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        leading: InkWell(
          onTap: () => Navigator.pop(context),
          child: Padding(
            padding: const EdgeInsets.only(left: 20, top: 5),
            child: CircleAvatar(
              backgroundColor: AppStyles.themeColor.withOpacity(.2),
              child: const Icon(
                Icons.arrow_back,
                color: Colors.red,
                size: 17,
              ),
            ),
          ),
        ),
        title: const Text(
          "Select currency from the list",
          style: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.w500,
            color: Colors.black87,
          ),
        ),
      ),
      body: Column(
        children: [
          SearchBar(
              color: Theme.of(context).scaffoldBackgroundColor,
              filterCurrencies: filterCurrencies),
          Expanded(
            child: ValueListenableBuilder(
                valueListenable: currencyList,
                builder: (context, currencyList, child) {
                  return ListView.separated(
                    separatorBuilder: (BuildContext context, int index) {
                      return const Divider();
                    },
                    padding: const EdgeInsets.symmetric(
                        horizontal: 25, vertical: 10),
                    itemCount: currencyList!.length,
                    itemBuilder: (context, index) {
                      var item = currencyList.reversed.toList()[index];
                      return ListTile(
                        visualDensity: VisualDensity.comfortable,
                        onTap: () {
                          Navigator.pop(context, item);
                        },
                        leading: Text(
                          item.name,
                          style: const TextStyle(
                            color: Colors.white,
                            fontSize: 17,
                            fontWeight: FontWeight.w300,
                          ),
                        ),
                        title: Chip(
                          padding: const EdgeInsets.symmetric(vertical: 0),
                          visualDensity: VisualDensity.compact,
                          labelPadding: const EdgeInsets.symmetric(
                              vertical: 0, horizontal: 15),
                          backgroundColor: AppStyles.themeColor.withOpacity(.8),
                          label: Text(
                            item.notation,
                            style: const TextStyle(
                              color: Colors.white,
                              fontSize: 10,
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ),
                        trailing: const Icon(
                          Icons.chevron_right,
                          color: Colors.white,
                          size: 17,
                        ),
                      );
                    },
                  );
                }),
          ),
        ],
      ),
    );
  }
}
