import 'package:flutter/cupertino.dart';

import '../utils/includes.dart';

class HomeScreen extends StatefulWidget {
  static String routeName = "home";
  final Currency upperSection;
  final Currency lowerSection;
  final bool isWhite;

  const HomeScreen({
    super.key,
    required this.upperSection,
    required this.lowerSection,
    required this.isWhite,
  });

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  ValueNotifier<Currency?> upperSectionData = ValueNotifier(null);
  ValueNotifier<Currency?> lowerSectionData = ValueNotifier(null);

  @override
  Widget build(BuildContext context) {
    upperSectionData.value = upperSectionData.value ?? widget.upperSection;
    lowerSectionData.value = lowerSectionData.value ?? widget.lowerSection;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppStyles.themeColor.withOpacity(.85),
        toolbarHeight: 0,
        elevation: 0,
        systemOverlayStyle: SystemUiOverlayStyle(
          statusBarColor: AppStyles.themeColor.withOpacity(.5),
          statusBarIconBrightness: Brightness.light,
        ),
      ),
      body: SafeArea(
        child: Stack(
          fit: StackFit.expand,
          children: [
            Column(
              children: [
                Expanded(
                  child: Container(
                    color: AppStyles.themeColor,
                    width: double.infinity,
                    padding:
                        const EdgeInsets.only(bottom: 30, left: 20, right: 20),
                    child: ValueListenableBuilder<Currency?>(
                        valueListenable: upperSectionData,
                        builder: (context, currency, child) {
                          return Column(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              InputChip(
                                labelPadding:
                                    const EdgeInsets.symmetric(horizontal: 20),
                                backgroundColor:
                                    AppStyles.themeColor.withOpacity(.9),
                                onPressed: () async {
                                  var result = await Navigator.push(
                                      context,
                                      CupertinoPageRoute(
                                          builder: (context) =>
                                              const RedCurrencyList()));
                                  if (result != null) {
                                    upperSectionData.value = result;
                                  }
                                },
                                label: Text(
                                  currency!.name,
                                  style: AppStyles.whiteTextStyle,
                                ),
                              ),
                              InkWell(
                                onTap: () {
                                  Navigator.pushReplacement(
                                    context,
                                    CupertinoPageRoute(
                                      fullscreenDialog: true,
                                      builder: (context) => RedInputScreen(
                                        fromCurrency: upperSectionData.value!,
                                        toCurrency: lowerSectionData.value!,
                                      ),
                                    ),
                                  );
                                },
                                child: Text(
                                  currency.value.toStringAsFixed(1),
                                  style: Theme.of(context)
                                      .textTheme
                                      .headlineMedium!
                                      .copyWith(color: Colors.white),
                                ),
                              ),
                              Text(
                                currency.notation.toUpperCase(),
                                style: Theme.of(context)
                                    .textTheme
                                    .headlineSmall
                                    ?.copyWith(color: Colors.white),
                              ),
                            ],
                          );
                        }),
                  ),
                ),
                Expanded(
                  child: Container(
                    width: double.infinity,
                    padding:
                        const EdgeInsets.only(top: 30, left: 20, right: 20),
                    color: Colors.white,
                    child: ValueListenableBuilder<Currency?>(
                        valueListenable: lowerSectionData,
                        builder: (context, currency, _) {
                          return Column(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Text(
                                currency!.notation.toUpperCase(),
                                style:
                                    Theme.of(context).textTheme.headlineSmall,
                              ),
                              InkWell(
                                onTap: () {
                                  Navigator.pushReplacement(
                                      context,
                                      CupertinoPageRoute(
                                          fullscreenDialog: true,
                                          builder: (context) =>
                                              WhiteInputScreen(
                                                fromCurrency:
                                                    lowerSectionData.value!,
                                                toCurrency:
                                                    upperSectionData.value!,
                                              )));
                                },
                                child: Text(
                                  currency.value.toStringAsFixed(1),
                                  style: Theme.of(context)
                                      .textTheme
                                      .headlineLarge!
                                      .copyWith(color: Colors.black87),
                                ),
                              ),
                              InputChip(
                                labelPadding:
                                    const EdgeInsets.symmetric(horizontal: 20),
                                backgroundColor: Colors.grey.shade50,
                                onPressed: () async {
                                  var result = await Navigator.push(
                                      context,
                                      CupertinoPageRoute(
                                          builder: (context) =>
                                              const WhiteCurrencyList()));
                                  if (result != null) {
                                    lowerSectionData.value = result;
                                  }
                                },
                                label: Text(
                                  currency.name,
                                  style: AppStyles.blackTextStyle,
                                ),
                              ),
                            ],
                          );
                        }),
                  ),
                ),
              ],
            ),
            Align(
              alignment: Alignment.center,
              child: Container(
                padding: const EdgeInsets.all(5),
                width: 60,
                height: 60,
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.white,
                    border: Border.all(color: AppStyles.themeColor, width: 3)),
                child: widget.isWhite
                    ? Icon(Icons.arrow_upward, color: AppStyles.themeColor)
                    : Icon(Icons.arrow_downward, color: AppStyles.themeColor),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
