import '../utils/includes.dart';

Map<String, Widget Function(BuildContext)> routes = {
  HomeScreen.routeName: (context) => HomeScreen(
        upperSection: Currency.currencyList.firstWhere((e) => e.id == 8),
        lowerSection: Currency.currencyList.firstWhere((e) => e.id == 3),
        isWhite: false,
      )
  // WhiteInputScreen.routeName: (context) =>
  //     const WhiteInputScreen(fromCurrency: ,)
};
