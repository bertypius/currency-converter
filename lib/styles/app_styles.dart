import 'package:flutter/material.dart';

class AppStyles {
  static TextStyle whiteTextStyle = const TextStyle(
      fontSize: 20, fontWeight: FontWeight.w300, color: Colors.white);
  static TextStyle blackTextStyle = const TextStyle(
      fontSize: 20, fontWeight: FontWeight.w300, color: Colors.black);
  static Color themeColor = const Color(0xffec5759);
}
